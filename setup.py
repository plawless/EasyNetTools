from distutils.core import setup
setup(name='EasyNetTools', 
	version='0.1', 
	description='A tool for making things quicker',
	author='Patrick Lawless',
	url='https://gitlab.com/plawless/EasyNetTools',
	packages=['EasyNetTools'], 
#	requires=['paramiko', 'netaddr'],
	)
